#ifndef ENTITY_H
#define ENTITY_H

#include <QtDebug>
#include <QPainter>

class Window;

class Entity
{
public:
    // Constructors
    Entity(QPoint position, QSize size, Window &window);
    Entity(QPoint position, QSize size, QPixmap image, Window &window);
    Entity(QPoint position, QSize size, QString text, Window &window);
    // Destructor
    virtual ~Entity();
    // Methods
    virtual void update() = 0;
    inline void repaint(QPainter &painter) {
        if(!image.isNull())
            painter.drawPixmap(QRect(position,size),image);
        else if(!text.isNull()) {
            painter.drawText(QRect(position,size), Qt::AlignCenter, text);
            painter.drawRect(QRect(position,size));
        } else
            painter.drawRect(QRect(position,size));
    }
    inline void move(int dx, int dy) {
        position = QPoint(position.x()+dx, position.y()+dy);
    }
    inline void moveTo(int new_x, int new_y) {
        position = QPoint(new_x,new_y);
    }
    // Getters and Setters
    Window &getWINDOW() const;
    QPoint getPosition() const;
    void setPosition(const QPoint &value);
    QSize getSize() const;
    void setSize(const QSize &value);
    QRect getRect() const;
    QPixmap getImage() const;
    void setImage(const QPixmap &value);
    QString getText() const;
    void setText(const QString &value);

protected:
    // Attributes
    Window &WINDOW;
    QPoint position;
    QSize size;
    QPixmap image;
    QString text;
};

#endif // ENTITY_H
