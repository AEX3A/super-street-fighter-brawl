#include "entity.h"
#include "game.h"

// Constructors
Entity::Entity(QPoint position, QSize size, Window &window)
: WINDOW(window)
{
    qDebug() << "Creation d'un objet Entity";
    this->position = position;
    this->size = size;
}

Entity::Entity(QPoint position, QSize size, QPixmap image, Window &window)
: WINDOW(window)
{
    qDebug() << "Creation d'un objet Entity";
    this->position = position;
    this->size = size;
    this->image = image;
}

Entity::Entity(QPoint position, QSize size, QString text, Window &window)
: WINDOW(window)
{
    qDebug() << "Creation d'un objet Entity";
    this->position = position;
    this->size = size;
    this->text = text;
}

// Destructor
Entity::~Entity()
{
    qDebug() << "Suppression d'un objet Entity";
}

// Getters and Setters
Window &Entity::getWINDOW() const
{
    return WINDOW;
}

QPoint Entity::getPosition() const
{
    return position;
}

void Entity::setPosition(const QPoint &value)
{
    position = value;
}

QSize Entity::getSize() const
{
    return size;
}

void Entity::setSize(const QSize &value)
{
    size = value;
}

QRect Entity::getRect() const
{
    return QRect(position,size);
}

QPixmap Entity::getImage() const
{
    return image;
}

void Entity::setImage(const QPixmap &value)
{
    image = value;
}
