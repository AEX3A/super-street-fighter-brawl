#ifndef SHIELD_WIELDER_H
#define SHIELD_WIELDER_H

#include "character.h"
#include "shield.h"
#include <QElapsedTimer>

class Shield_Wielder : public Character
{
public:
    // Constructors
    // initialisation par défaut de Character et Shield_Wielder
    Shield_Wielder(QPoint position, QSize size, Window &window);
    // initialisation des attributs d'une classe + initialisation par défaut de l'autre classe
    Shield_Wielder(QPoint position, QSize size, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    Shield_Wielder(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window);
    // initialisation des attributs des 2 classes
    Shield_Wielder(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    // Destructor
    ~Shield_Wielder();
    // Methods
    virtual void manage_inputs() override;
    // Getters and Setters
    double getShield_protection() const;
    void setShield_protection(double value);
    double getShield_cooldown() const;
    void setShield_cooldown(double value);
    int getShield_input() const;
    void setShield_input(int value);
    bool getIs_wielding_shield() const;
    void setIs_wielding_shield(bool value);

private:
    // Attributes
    double shield_protection;
    double shield_cooldown;
    int shield_input;
    bool is_wielding_shield;
    QElapsedTimer shield_cooldown_delay;
};

#endif // SHIELD_WIELDER_H
