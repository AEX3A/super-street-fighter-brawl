#include "game.h"

// Constructors
Game::Game(QSize window_size) : QWidget(), window(Window(*this))
{
    qDebug() << "Creation d'un objet Game";
    window_size != QSize(0,0) ? setFixedSize(window_size) : setWindowState(Qt::WindowFullScreen);
    setWindowTitle("Super Street Fighter Brawl");
    setWindowIcon(QIcon(":/Interface_Sprites/Sprites/Interface/VS.png"));

    game_paused = false;
    game_over = false;
    window.init_game();
    connect(&update_timer, SIGNAL(timeout()), this, SLOT(update_game()));
    update_timer.start(1000/FPS);
    show();
}

// Destructor
Game::~Game()
{
    qDebug() << "Suppression d'un objet Game";
}

// Methods
void Game::update_game()
{
    qDebug() << "===================================";
    qDebug() << "MOVING GAME ENTITIES";
    window.update_game();
    qDebug() << "===================================";
    qDebug() << Qt::endl;
    repaint();
}

void Game::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    
    qDebug() << "===================================";
    qDebug() << "DRAWING GAME ENTITIES";
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setRenderHint(QPainter::TextAntialiasing);
    window.repaint_game(painter);
    painter.end();
    qDebug() << "===================================";
    qDebug() << Qt::endl;
}

void Game::keyPressEvent(QKeyEvent *event)
{
    keys[event->key()] = true;
    QWidget::keyPressEvent(event);
}

void Game::keyReleaseEvent(QKeyEvent *event)
{
    keys[event->key()] = false;
    QWidget::keyReleaseEvent(event);
}

// Getters and Setters
int Game::getFPS() const
{
    return FPS;
}

QMap<int, bool> const& Game::getKeys() const
{
    return keys;
}

bool const& Game::getGame_paused() const
{
    return game_paused;
}

void Game::setGame_paused(bool value)
{
    game_paused = value;
}

bool const& Game::getGame_over() const
{
    return game_over;
}

void Game::setGame_over(bool value)
{
    game_over = value;
}

Window Game::getWindow() const
{
    return window;
}



