#include "character.h"
#include "game.h"

// Constructors
Character::Character(QPoint position, QSize size, Window &window)
: Entity(position,size,window)
{
    qDebug() << "Creation d'un objet Character";
    this->name = "name not defined";
    this->skin = "DBZ_Character1";
    this->max_hp = 100;
    this->hp = max_hp;
    this->move_speed = window.getGAME().size().width()*500/1920;
    this->jump_speed = window.getGAME().size().height()*1000/1080;
    this->gravity_speed = window.getGAME().size().height()*100/1080;
    this->velocity = QVector2D(0,0);
    this->is_on_ground = false;
    this->orientation = true;
    this->move_left_input = Qt::Key::Key_Left;
    this->move_right_input = Qt::Key::Key_Right;
    this->jump_input = Qt::Key::Key_Up;
}

Character::Character(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window)
: Entity(position,size,window)
{
    qDebug() << "Creation d'un objet Character";
    this->name = name;
    this->skin = skin;
    this->max_hp = max_hp;
    this->hp = max_hp;
    this->move_speed = move_speed;
    this->jump_speed = jump_speed;
    this->gravity_speed = gravity_speed;
    this->velocity = QVector2D(0,0);
    this->is_on_ground = false;
    this->orientation = orientation;
    this->move_left_input = move_left_input;
    this->move_right_input = move_right_input;
    this->jump_input = jump_input;
}

// Destructor
Character::~Character()
{
    qDebug() << "Suppression d'un objet Character";
}

// Operators
bool Character::operator==(const Character &character) const
{
    return (name==character.getName() && skin==character.skin && max_hp == character.getMax_hp() && hp == character.getHp() && move_speed==character.getMove_speed() && jump_speed==character.getJump_speed() && gravity_speed==character.getGravity_speed() && velocity==character.getVelocity() && orientation==character.getOrientation() && is_on_ground==character.getIs_on_ground() && move_left_input==character.getMove_left_input() && move_right_input==character.getMove_right_input() && jump_input==character.getJump_input());
}

// Methods
void Character::update()
{
    velocity.setX(0);
    manage_inputs();
    manage_collisions();
    manage_animations();
    move(velocity.x(),velocity.y());

    qDebug() << "Velocity :" << velocity;
    qDebug() << "Position :" << position;
    qDebug() << "Orientation :" << QString(orientation ? "right" : "left");
    qDebug() << "Grounded ? :" << QString(is_on_ground ? "yes" : "no");
}

void Character::manage_inputs()
{
    if(WINDOW.getGAME().getKeys()[move_left_input]) {
        orientation = false;
        velocity.setX(velocity.x()-(move_speed/(double)WINDOW.getGAME().getFPS()));
    }
    if(WINDOW.getGAME().getKeys()[move_right_input]) {
        orientation = true;
        velocity.setX(velocity.x()+(move_speed/(double)WINDOW.getGAME().getFPS()));
    }

    if(is_on_ground) {
        if(WINDOW.getGAME().getKeys()[jump_input]) {
            velocity.setY(velocity.y()-(jump_speed/(double)WINDOW.getGAME().getFPS()));
            is_on_ground = false;
        }
    } else {
        velocity.setY(velocity.y()+(gravity_speed/(double)WINDOW.getGAME().getFPS()));
    }
}

void Character::manage_collisions()
{
    QRect my_rect = QRect(QPoint(position.x()+size.width()/2-(size.width()*3/8/2),position.y()+size.height()/2-(size.height()*5/8/2)),QSize(size.width()*3/8,size.height()*5/8));
    QPoint leg_hitbox[] = {QPoint(position.x()+size.width()*1/3, position.y()+size.height()), QPoint(position.x()+size.width()*2/3, position.y()+size.height())};

    for(Entity* entity : WINDOW.getGame_elements())
    {
        if(entity == this)
            continue;

        if(dynamic_cast<Character*> (entity) != nullptr) {
            Character * character = dynamic_cast<Character*> (entity);
            QRect character_rect = QRect(character->getPosition(),character->getSize());
            if(character_rect.intersects(my_rect)) {
                qDebug() << "Opponent detected by" << name.toLower();
                /*
                if(my_rect.right() >= character_rect.left() && velocity.x() > 0) {
                    velocity.setX(0);
                    //moveTo(WINDOW.getGAME().size().width()-size.width()*5/8, position.y());
                }
                if(my_rect.left() <= character_rect.right() && velocity.x() < 0) {
                    velocity.setX(0);
                    //moveTo(WINDOW.getGAME().size().width()-size.width()*5/8, position.y());
                }
                if(my_rect.top() <= character_rect.bottom() && velocity.y() < 0) {
                    velocity.setY(0);
                    //moveTo(WINDOW.getGAME().size().width()-size.width()*5/8, position.y());
                }
                if(my_rect.bottom() >= character_rect.top() && velocity.y() > 0) {
                    velocity.setY(0);
                    //moveTo(WINDOW.getGAME().size().width()-size.width()*5/8, position.y());
                }
                */
            }
        } else if(dynamic_cast<Platform*> (entity) != nullptr) {
            Platform* platform = dynamic_cast<Platform*> (entity);
            if(platform->getRect().intersects(getRect())) {
                qDebug() << "Platform detected by" << name.toLower();
                if(platform->getRect().contains(leg_hitbox[0]) || platform->getRect().contains(leg_hitbox[1])) {
                    if(velocity.y() >= 0) {
                        velocity.setY(0);
                        moveTo(position.x(), platform->getPosition().y()-size.height()*7/8);
                        is_on_ground = true;
                    }
                } else {
                    is_on_ground = false;
                }
            }
        } else if(dynamic_cast<Projectile*> (entity) != nullptr) {
            Projectile* projectile = dynamic_cast<Projectile*> (entity);
            if((projectile->getRect().intersects(my_rect)) && !(projectile->getCASTER() == static_cast<Character>(*this) )) {
                qDebug() << "Projectile detected by" << name.toLower();
                setHp(hp - projectile->getDamage());
                projectile->explode();
            }
        }
    }

    if((position.x()+size.width()*4/5) >= WINDOW.getGAME().size().width()) {
        if(velocity.x() > 0) {
            velocity.setX(0);
            moveTo(WINDOW.getGAME().size().width()-size.width()*4/5, position.y());
        }
    }
    if(position.x()+size.width()*1/5 <= 0) {
        if(velocity.x() < 0) {
            velocity.setX(0);
            moveTo(-size.width()*1/5, position.y());
        }
    }
    if(position.y()+size.height()*1/5 <= 0) {
        if(velocity.y() < 0) {
            velocity.setY(0);
            moveTo(position.x(), -size.height()*1/20);
        }
    }
}

void Character::manage_animations()
{
    if(!is_on_ground) {
        if(orientation)
            image = QPixmap(":/Character_Sprites/Sprites/Characters/"+skin+"/Slices/slice_2_15.png");
        else
            image = QPixmap(":/Character_Sprites/Sprites/Characters/"+skin+"/Slices/slice_2_15.png").transformed(QTransform().scale(-1,1));
    } else {
        if(orientation)
            image = QPixmap(":/Character_Sprites/Sprites/Characters/"+skin+"/Slices/slice_1_1.png");
        else
            image = QPixmap(":/Character_Sprites/Sprites/Characters/"+skin+"/Slices/slice_1_1.png").transformed(QTransform().scale(-1,1));
    }
}

// Getters and Setters
QString Character::getName() const
{
    return name;
}

void Character::setName(const QString &value)
{
    name = value;
}

QString Character::getSkin() const
{
    return skin;
}

void Character::setSkin(const QString &value)
{
    skin = value;
}

double Character::getMax_hp() const
{
    return max_hp;
}

void Character::setMax_hp(double value)
{
    max_hp = value;
}

double Character::getHp() const
{
    return hp;
}

void Character::setHp(double value)
{
    hp = value;
}

double Character::getMove_speed() const
{
    return move_speed;
}

void Character::setMove_speed(double value)
{
    move_speed = value;
}

double Character::getJump_speed() const
{
    return jump_speed;
}

void Character::setJump_speed(double value)
{
    jump_speed = value;
}

double Character::getGravity_speed() const
{
    return gravity_speed;
}

void Character::setGravity_speed(double value)
{
    gravity_speed = value;
}

QVector2D Character::getVelocity() const
{
    return velocity;
}

void Character::setVelocity(const QVector2D &value)
{
    velocity = value;
}

bool Character::getIs_on_ground() const
{
    return is_on_ground;
}

void Character::setIs_on_ground(bool value)
{
    is_on_ground = value;
}

bool Character::getOrientation() const
{
    return orientation;
}

void Character::setOrientation(bool value)
{
    orientation = value;
}

int Character::getMove_left_input() const
{
    return move_left_input;
}

void Character::setMove_left_input(int value)
{
    move_left_input = value;
}

int Character::getMove_right_input() const
{
    return move_right_input;
}

void Character::setMove_right_input(int value)
{
    move_right_input = value;
}

int Character::getJump_input() const
{
    return jump_input;
}

void Character::setJump_input(int value)
{
    jump_input = value;
}
