#ifndef SHIELD_H
#define SHIELD_H

#include "entity.h"
#include "character.h"

class Shield : public Entity
{
public:
    // Constructors
    Shield(QPoint position, QSize size, Character &wielder, Window &window);
    Shield(QPoint position, QSize size, QPixmap image, Character &wielder, Window &window);
    // Destructor
    ~Shield();
    // Methods
    void update() override;
    // Getters and Setters
    Character &getWIELDER() const;

private:
    // Attributes
    Character &WIELDER;
};

#endif // SHIELD_H
