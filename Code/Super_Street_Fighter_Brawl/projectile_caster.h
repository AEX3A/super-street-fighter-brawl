#ifndef PROJECTILE_CASTER_H
#define PROJECTILE_CASTER_H

#include "character.h"
#include "projectile.h"
#include <QElapsedTimer>

class Projectile_Caster : public Character
{
public:
    // Constructors
    // initialisation par défaut de Character et Projectile_Caster
    Projectile_Caster(QPoint position, QSize size, Window &window);
    // initialisation des attributs d'une classe + initialisation par défaut de l'autre classe
    Projectile_Caster(QPoint position, QSize size, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window);
    Projectile_Caster(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window);
    // initialisation des attributs des 2 classes
    Projectile_Caster(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window);
    // Destructor
    ~Projectile_Caster();
    // Methods
    virtual void manage_inputs() override;
    // Getters and Setters
    double getProjectile_speed() const;
    void setProjectile_speed(double value);
    double getProjectile_damage() const;
    void setProjectile_damage(double value);
    double getProjectile_cooldown() const;
    void setProjectile_cooldown(double value);
    int getProjectile_input() const;
    void setProjectile_input(int value);

private:
    // Attributes
    double projectile_speed;
    double projectile_damage;
    double projectile_cooldown;
    int projectile_input;
    QElapsedTimer cast_cooldown_delay;
};

#endif // PROJECTILE_CASTER_H
