#include "background.h"
#include "game.h"

// Constructors
Background::Background(QPixmap image, Window &window)
: Entity(QPoint(0,0),window.getGAME().size(),image,window)
{
    qDebug() << "Creation d'un objet Background";
}

// Destructor
Background::~Background()
{
    qDebug() << "Suppression d'un objet Background";
}

// Methods
void Background::update()
{

}


