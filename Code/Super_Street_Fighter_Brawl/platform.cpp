#include "platform.h"
#include "game.h"

// Constructors
Platform::Platform(QPoint position, QSize size, Window &window)
: Entity(position,size,window)
{
    qDebug() << "Creation d'un objet Platform";
}

Platform::Platform(QPoint position, QSize size, QPixmap image, Window &window) : Entity(position,size,image,window)
{
    qDebug() << "Creation d'un objet Platform";
}

// Destructor
Platform::~Platform()
{
    qDebug() << "Suppression d'un objet Platform";
}

// Methods
void Platform::update()
{

}
