#include "mage.h"

Mage::Mage(QPoint position, QSize size, Window &window)
: Projectile_Caster(position,size,window), Shield_Wielder(position,size,window)
{
    qDebug() << "Creation d'un objet Mage";
}

Mage::Mage(QPoint position, QSize size, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window)
: Projectile_Caster(position,size,projectile_speed,projectile_damage,projectile_cooldown,projectile_input,window), Shield_Wielder(position,size,window)
{
    qDebug() << "Creation d'un objet Mage";
}

Mage::Mage(QPoint position, QSize size, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Projectile_Caster(position,size,window), Shield_Wielder(position,size,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Mage";
}

Mage::Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window)
: Projectile_Caster(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Mage";
}

Mage::Mage(QPoint position, QSize size, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Projectile_Caster(position,size,projectile_speed,projectile_damage,projectile_cooldown,projectile_input,window), Shield_Wielder(position,size,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Mage";
}

Mage::Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window)
: Projectile_Caster(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,projectile_speed,projectile_damage,projectile_cooldown,projectile_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Mage";
}

Mage::Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Projectile_Caster(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Mage";
}

Mage::Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Projectile_Caster(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,projectile_speed,projectile_damage,projectile_cooldown,projectile_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Mage";
}

// Destructor
Mage::~Mage()
{
    qDebug() << "Suppression d'un objet Mage";
}
