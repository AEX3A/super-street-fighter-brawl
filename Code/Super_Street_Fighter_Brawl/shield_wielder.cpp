#include "shield_wielder.h"
#include "game.h"

// Constructors
Shield_Wielder::Shield_Wielder(QPoint position, QSize size, Window &window)
: Character(position,size,window)
{
    qDebug() << "Creation d'un objet Shield Wielder";
    this->shield_protection = 80;
    this->shield_cooldown = 250;
    this->shield_input = Qt::Key::Key_Control;
    this->is_wielding_shield = false;
}

Shield_Wielder::Shield_Wielder(QPoint position, QSize size, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Character(position,size,window)
{
    qDebug() << "Creation d'un objet Shield Wielder";
    this->shield_protection = shield_protection;
    this->shield_cooldown = shield_cooldown;
    this->shield_input = shield_input;
    this->is_wielding_shield = false;
}

Shield_Wielder::Shield_Wielder(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window)
: Character(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Shield Wielder";
    this->shield_protection = 80;
    this->shield_cooldown = 250;
    this->shield_input = Qt::Key::Key_Control;
    this->is_wielding_shield = false;
}

Shield_Wielder::Shield_Wielder(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Character(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Shield Wielder";
    this->shield_protection = shield_protection;
    this->shield_cooldown = shield_cooldown;
    this->shield_input = shield_input;
    this->is_wielding_shield = false;
}

// Destructor
Shield_Wielder::~Shield_Wielder()
{
    qDebug() << "Suppression d'un objet Shield Wielder";
}

// Methods
void Shield_Wielder::manage_inputs()
{
    Character::manage_inputs();

    if(shield_cooldown_delay.elapsed()>=shield_cooldown) {
        if(WINDOW.getGAME().getKeys()[shield_input]) {
            is_wielding_shield = true;
            bool shield_found = false;
            for(Entity* entity : WINDOW.getGame_elements())
            {
                if(dynamic_cast<Shield*> (entity) != nullptr) {
                    shield_found = true;
                }
            }
            if(!shield_found) {
                WINDOW.addTo_be_added(new Shield(QPoint(position.x()+size.width()/2-(size.width()*3/8/2),position.y()+size.height()/2-(size.height()*5/8/2)),QSize(size.width()*3/8,size.height()*5/8),*this,WINDOW));
            }
        } else {
            if(is_wielding_shield) {
                for(Entity* entity : WINDOW.getGame_elements())
                {
                    if(dynamic_cast<Shield*> (entity) != nullptr) {
                        Shield * shield = dynamic_cast<Shield*> (entity);
                        WINDOW.addTo_be_deleted(shield);
                    }
                }
                shield_cooldown_delay.restart();
            }
            is_wielding_shield = false;
        }
    }
}

// Getters and Setters
double Shield_Wielder::getShield_protection() const
{
    return shield_protection;
}

void Shield_Wielder::setShield_protection(double value)
{
    shield_protection = value;
}

int Shield_Wielder::getShield_input() const
{
    return shield_input;
}

void Shield_Wielder::setShield_input(int value)
{
    shield_input = value;
}

bool Shield_Wielder::getIs_wielding_shield() const
{
    return is_wielding_shield;
}

void Shield_Wielder::setIs_wielding_shield(bool value)
{
    is_wielding_shield = value;
}

double Shield_Wielder::getShield_cooldown() const
{
    return shield_cooldown;
}

void Shield_Wielder::setShield_cooldown(double value)
{
    shield_cooldown = value;
}

