#ifndef TEXT_H
#define TEXT_H

#include "entity.h"

class Text : public Entity
{
public:
    // Constructors
    Text(QPoint position, QSize size, QString text, Window & window);
    // Destructor
    ~Text();
};

#endif // TEXT_H
