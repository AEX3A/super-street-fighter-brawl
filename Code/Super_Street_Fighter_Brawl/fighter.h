#ifndef FIGHTER_H
#define FIGHTER_H

#include "kick_user.h"
#include "shield_wielder.h"

class Fighter : public Kick_User, public Shield_Wielder
{
public:
    // Constructors
    // initialisation par défaut de Character, Kick_User et Shield_Wielder
    Fighter(QPoint position, QSize size, Window &window);
    // initialisation des attributs d'une classe + initialisation par défaut des 2 autres classes
    Fighter(QPoint position, QSize size, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window);
    Fighter(QPoint position, QSize size, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window);
    // initialisation des attributs de 2 classes + initialisation par défaut de l'autre classe
    Fighter(QPoint position, QSize size, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window);
    Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    // initialisation des attributs de Character, Kick_User et Shield_Wielder
    Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    // Destructor
    ~Fighter();
};

#endif // FIGHTER_H
