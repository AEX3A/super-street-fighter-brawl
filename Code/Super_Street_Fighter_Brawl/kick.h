#ifndef KICK_H
#define KICK_H

#include "entity.h"
#include "character.h"

class Kick : public Entity
{
public:
    // Constructors
    Kick(QPoint position, QSize size, double damage, Character &kicker, Window &window);
    Kick(QPoint position, QSize size, QPixmap image, double damage, Character &kicker, Window &window);
    // Destructor
    ~Kick();
    // Methods
    void update() override;
    // Getters and Setters
    Character &getKICKER() const;
    double getDamage() const;
    void setDamage(double value);

private:
    // Attributes
    Character &KICKER;
    double damage;
};

#endif // KICK_H
