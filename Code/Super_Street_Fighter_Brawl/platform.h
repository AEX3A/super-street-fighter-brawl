#ifndef PLATFORM_H
#define PLATFORM_H

#include <QObject>
#include <QTimer>
#include <QVector2D>
#include <QtDebug>

#include "entity.h"

class Platform : public Entity
{
public:
    // Constructors
    Platform(QPoint position, QSize size, Window &window);
    Platform(QPoint position, QSize size, QPixmap image, Window &window);
    // Destructor
    ~Platform();
    // Methods
    void update() override;
};

#endif // PLATEFORME_H
