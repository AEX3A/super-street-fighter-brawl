#include "fighter.h"

// Constructors
Fighter::Fighter(QPoint position, QSize size, Window &window) : Kick_User(position,size,window), Shield_Wielder(position,size,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

Fighter::Fighter(QPoint position, QSize size, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window)
: Kick_User(position,size,kick_range,kick_damage,kick_cooldown,kick_duration,kick_input,window), Shield_Wielder(position,size,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

Fighter::Fighter(QPoint position, QSize size, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Kick_User(position,size,window), Shield_Wielder(position,size,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

Fighter::Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window)
: Kick_User(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

Fighter::Fighter(QPoint position, QSize size, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Kick_User(position,size,kick_range,kick_damage,kick_cooldown,kick_duration,kick_input,window), Shield_Wielder(position,size,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

Fighter::Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window)
: Kick_User(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,kick_range,kick_damage,kick_cooldown,kick_duration,kick_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

Fighter::Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Kick_User(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

Fighter::Fighter(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, double shield_protection, double shield_cooldown, int shield_input, Window &window)
: Kick_User(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,kick_range,kick_damage,kick_cooldown,kick_duration,kick_input,window), Shield_Wielder(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,shield_protection,shield_cooldown,shield_input,window)
{
    qDebug() << "Creation d'un objet Fighter";
}

// Destructor
Fighter::~Fighter()
{
    qDebug() << "Suppression d'un objet Fighter";
}
