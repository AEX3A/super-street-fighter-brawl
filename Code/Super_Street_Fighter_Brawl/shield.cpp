#include "shield.h"

// Constructors
Shield::Shield(QPoint position, QSize size, Character &wielder, Window & window)
: Entity(position,size,window), WIELDER(wielder)
{
    qDebug() << "Creation d'un objet Shield";
}

Shield::Shield(QPoint position, QSize size, QPixmap image, Character &wielder, Window & window)
: Entity(position,size,image,window), WIELDER(wielder)
{
    qDebug() << "Creation d'un objet Shield";
}

// Destructor
Shield::~Shield()
{
    qDebug() << "Suppression d'un objet Shield";
}

// Methods
void Shield::update()
{
    position = QPoint(WIELDER.getPosition().x()+WIELDER.getSize().width()/2-(WIELDER.getSize().width()*3/8/2),WIELDER.getPosition().y()+WIELDER.getSize().height()/2-(WIELDER.getSize().height()*5/8/2));
}

// Getters and Setters
Character &Shield::getWIELDER() const
{
    return WIELDER;
}
