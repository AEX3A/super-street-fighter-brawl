#ifndef MAGE_H
#define MAGE_H

#include "projectile_caster.h"
#include "shield_wielder.h"

class Mage : public Projectile_Caster, public Shield_Wielder
{
public:
    // Constructors
    // initialisation par défaut de Character, Projectile_Caster et Shield_Wielder
    Mage(QPoint position, QSize size, Window &window);
    // initialisation des attributs d'une classe + initialisation par défaut des 2 autres classes
    Mage(QPoint position, QSize size, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window);
    Mage(QPoint position, QSize size, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window);
    // initialisation des attributs de 2 classes + initialisation par défaut de l'autre classe
    Mage(QPoint position, QSize size, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window);
    Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    // initialisation des attributs de Character, Projectile_Caster et Shield_Wielder
    Mage(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, double shield_protection, double shield_cooldown, int shield_input, Window &window);
    // Destructor
    ~Mage();
};

#endif // MAGE_H
