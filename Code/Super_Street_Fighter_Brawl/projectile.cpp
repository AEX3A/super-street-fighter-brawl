#include "projectile.h"
#include "game.h"

// Constructors
Projectile::Projectile(QPoint position, QSize size, double damage, double speed, bool direction, Character &caster, Window &window)
: Entity(position,size,window), CASTER(caster)
{
    qDebug() << "Creation d'un objet Projectile";
    this->damage = damage;
    this->speed = window.getGAME().size().width()*speed/1920;
    this->direction = direction;
}

Projectile::Projectile(QPoint position, QSize size, QPixmap image, double damage, double speed, bool direction, Character &caster, Window & window)
: Entity(position,size,image,window), CASTER(caster)
{
    qDebug() << "Creation d'un objet Projectile";
    this->damage = damage;
    this->speed = window.getGAME().size().width()*speed/1920;
    this->direction = direction;
}

// Destructor
Projectile::~Projectile()
{
    qDebug() << "Suppression d'un objet Projectile";
}

// Methods
void Projectile::update()
{
    qDebug() << "PROJECTILLE POSITION " << position;
    qDebug() << "SPEED " << speed;
    move(direction ? speed/(double)WINDOW.getGAME().getFPS() : -(speed/(double)WINDOW.getGAME().getFPS()), 0);
    if((position.x()+size.width() <= 0 || position.x() >= WINDOW.getGAME().size().width())) {
        explode();
    }
}

void Projectile::explode()
{
    WINDOW.addTo_be_deleted(this);
}

// Getters and Setters
Character &Projectile::getCASTER() const
{
    return CASTER;
}

double Projectile::getDamage() const
{
    return damage;
}

void Projectile::setDamage(double value)
{
    damage = value;
}

double Projectile::getSpeed() const
{
    return speed;
}

void Projectile::setSpeed(double value)
{
    speed = value;
}

bool Projectile::getDirection() const
{
    return direction;
}

void Projectile::setDirection(bool value)
{
    direction = value;
}
